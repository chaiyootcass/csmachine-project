import 'dart:math';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:threading/threading.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ProjectML',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Path Finder'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class Block {
  double value;
  Color colorBackground;
  String icon;
  Block({this.value = 0, this.colorBackground, this.icon = "null"});
}

List<List<Block>> table;
List<List<int>> R;
List<List<double>> Q;
double BOX_SIZE = 80;
double learingRate = 0.8;
int size = 4, setSomething = 0;
int rewardC = 3,
    rewardR = 1,
    punishC = 3,
    punishR = 0,
    blockR = 1,
    blockC = 1,
    block2R = 2,
    block2C = 1;
int len = 0, action = 0;
var rd = new Random();
int currentR = 3, currentC = 0, setR = 3, setC = 0;
double max(List<double> s) {
  double max = 0;
  for (double tem in s) {
    if (tem > max) {
      max = tem;
    }
  }
  return max;
}

int chooseState(int x) {
  double max = -1;
  List<double> s = Q[x];
  List<int> temChoose = new List();
  for (double tem in s) {
    if (tem > max) {
      max = tem;
    }
  }
  for (int i = 0; i < s.length; i++) {
    if (max == s[i]) {
      temChoose.add(i);
    }
  }
  if (temChoose.length <= 4) {
    return temChoose[rd.nextInt(temChoose.length)];
  } else {
    int max = -1;
    List<int> s = R[x];
    temChoose.clear();
    for (int tem in s) {
      if (tem > max) {
        max = tem;
      }
    }
    for (int i = 0; i < s.length; i++) {
      if (max == s[i]) {
        temChoose.add(i);
      }
    }
    return temChoose[rd.nextInt(temChoose.length)];
  }
}

int randomPath(List<int> r) {
  List<int> tem = new List();
  for (int i = 0; i < r.length; i++) {
    if (r[i] != -1) {
      tem.add(i);
    }
  }
  return tem[rd.nextInt(tem.length)];
}

void setReward() {
  Q = new List();
  R = new List();
  for (int i = 0; i < len; i++) {
    List<double> tem = List();
    List<int> temI = List();
    for (int l = 0; l < len; l++) {
      tem.add(0);
      temI.add(-1);
    }
    Q.add(tem);
    R.add(temI);
  }
  int nR = 0, nC = 0;
  int b1 = -1, b2 = -1;
  bool check = false;
  for (int i = 0; i < size; i++) {
    if (check) break;
    for (int l = 0; l < size; l++) {
      b1++;
      if (i == blockR && l == blockC) {
        check = true;
        break;
      }
    }
  }
  check = false;
  for (int i = 0; i < size; i++) {
    if (check) break;
    for (int l = 0; l < size; l++) {
      b2++;
      if (i == block2R && l == block2C) {
        check = true;
        break;
      }
    }
  }
  for (int i = 0; i < len; i++) {
    if (i == b1 || i == b2) {
      //i!=block
    } else {
      List<int> tem = new List();
      int top = i - action, dow = i + action, left = i - 1, right = i + 1;
      tem.add(top);
      tem.add(dow);
      if ((i % action) != (table.length - 1)) {
        tem.add(right);
      }
      if (i % table.length != 0) {
        tem.add(left);
      }
      for (int x in tem) {
        if (x >= 0 && x < len) {
          nC = x % table.length;
          nR = (x / table.length).floor();
          if (table[nR][nC].icon == 'null' || table[nR][nC].icon == 'play') {
            R[i][x] = 0;
          }
          if (table[nR][nC].icon == '+1') {
            R[i][x] = 100;
          }
          if (table[nR][nC].icon == '-1') {
            R[i][x] = -100;
          }
          if (table[nR][nC].icon == 'block' || table[nR][nC].icon == 'block2') {
            R[i][x] = -1;
          }
        }
      }
    }
  }
}

class _MyHomePageState extends State<MyHomePage> {
  StreamController<String> textStatus = StreamController<String>();
  final myController = TextEditingController();

  List<Row> buildTable() {
    List<Row> listRow = List();
    for (int row = 0; row < 4; row++) {
      listRow.add(Row(
          mainAxisSize: MainAxisSize.min, children: buildRowBlockUnit(row)));
    }
    return listRow;
  }

  Widget showIcon(String icon, String value) {
    if (icon == 'null') {
      return Center(child: Text("" + value));
    } else {
      if (icon == 'play') {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[Text("" + value), Icon(Icons.place)],
          ),
        );
      }
      if (icon == 'block') {
        return Center(
          child: Icon(Icons.do_not_disturb),
        );
      }
      if (icon == 'block2') {
        return Center(
          child: Icon(Icons.highlight_off),
        );
      }
      if (icon == '+1') {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[Text('+1'), Icon(Icons.sentiment_satisfied)],
          ),
        );
      }
      if (icon == '-1') {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('-1'),
              Icon(Icons.sentiment_very_dissatisfied)
            ],
          ),
        );
      }
    }
    return Center(
      child: Text(" "),
    );
  }

  Widget buildBlockUnit(int row, int col) {
    return GestureDetector(
      onTap: () {
        if (table[row][col].icon == 'null') {
          setState(() {
            if (setSomething == -1) {
              table[punishR][punishC].icon = 'null';
              table[row][col].icon = '-1';
              punishC = col;
              punishR = row;
            }
            if (setSomething == 1) {
              table[rewardR][rewardC].icon = 'null';
              table[row][col].icon = '+1';
              rewardC = col;
              rewardR = row;
            }
            if (setSomething == 99) {
              table[currentR][currentC].icon = 'null';
              table[row][col].icon = 'play';
              currentR = row;
              currentC = col;
              setC = col;
              setR = row;
            }
            if (setSomething == 81) {
              table[blockR][blockC].icon = 'null';
              table[row][col].icon = 'block';
              blockR = row;
              blockC = col;
            }
            if (setSomething == 82) {
              table[block2R][block2C].icon = 'null';
              table[row][col].icon = 'block2';
              block2R = row;
              block2C = col;
            }
            if (setSomething == 1) {
              int forR = -1;
              bool check = false;
              for (int i = 0; i < size; i++) {
                if (check) {
                  break;
                }
                for (int l = 0; l < size; l++) {
                  forR++;
                  if (i == row && l == col) {
                    R[forR][forR] = 100;
                    check = true;
                    break;
                  }
                }
              }
            }
            setReward();
            setSomething = 0;
            for (int row = 0; row < size; row++) {
              for (int col = 0; col < size; col++) {
                table[row][col].value = 0;
              }
            }
            for (int i = 0; i < len; i++) {
              for (int l = 0; l < len; l++) {
                Q[i][l] = 0;
              }
            }
          });
          textStatus.sink.add('Set Finish');
        } else {
          textStatus.sink.add('Set Another!!');
        }
      },
      child: Container(
        decoration: BoxDecoration(
          color: table[row][col].colorBackground,
          borderRadius: BorderRadius.circular(4),
        ),
        width: BOX_SIZE,
        height: BOX_SIZE,
        margin: EdgeInsets.all(3),
        child: showIcon(
            table[row][col].icon, table[row][col].value.toStringAsFixed(3)),
      ),
    );
  }

  void _randomPositon() async {
    setState(() {
      table[currentR][currentC].icon = 'null';
      int r = 0;
      int c = 0;
      while (true) {
        r = rd.nextInt(4);
        c = rd.nextInt(4);
        if (table[r][c].icon == 'null') {
          currentC = c;
          currentR = r;
          setC=c;
          setR=r;
          table[r][c].icon = 'play';
          break;
        }
      }
    });
  }

  void playOnetime() async {
    for (int i = 0; i < 1; i++) {
      int State = (currentC % table.length) + (currentR * table.length);
      while (true) {
        await Thread.sleep(200);
        setState(() {
          table[currentR][currentC].icon = 'null';
        });
        int Path = randomPath(R[State]);
        if (R[State][Path] == 100 || R[State][Path] == -100) {
          await Thread.sleep(100);
          setState(() {
            Q[State][Path] = R[State][Path] + (learingRate * max(Q[Path]));
            if (R[State][Path] == 100) {
              table[currentR][currentC].value = Q[State][Path];
              textStatus.sink.add('Found Reward');
            } else {
              textStatus.sink.add('Found Punnish');
            }
            currentC = setC;
            currentR = setR;
            table[currentR][currentC].icon = 'play';
          });
          break;
        }
        currentC = Path % size;
        currentR = (Path / size).floor();
        double m = max(Q[Path]);
        Q[State][Path] = R[State][Path] + learingRate * m;
        State = Path;
        await Thread.sleep(200);
        setState(() {
          table[currentR][currentC].icon = 'play';
          table[currentR][currentC].value = m;
        });
      }
    }
  }

  void playFast() {
    for (int i = 0; i < 1; i++) {
      int State = (currentC % table.length) + (currentR * table.length);
      while (true) {
        table[currentR][currentC].icon = 'null';
        int Path = randomPath(R[State]);
        if (R[State][Path] == 100 || R[State][Path] == -100) {
          setState(() {
            Q[State][Path] = R[State][Path] + (learingRate * max(Q[Path]));
            if (R[State][Path] == 100) {
              table[currentR][currentC].value = Q[State][Path];
              textStatus.sink.add('Found Reward');
            } else {
              textStatus.sink.add('Found Punnish');
            }
            currentC = setC;
            currentR = setR;
            table[currentR][currentC].icon = 'play';
          });
          break;
        }
        currentC = Path % size;
        currentR = (Path / size).floor();
        double m = max(Q[Path]);
        Q[State][Path] = R[State][Path] + learingRate * m;
        State = Path;
        table[currentR][currentC].value = m;
        table[currentR][currentC].icon = 'play';
      }
    }
  }

  List<Widget> buildRowBlockUnit(int row) {
    List<Widget> list = List();
    for (int col = 0; col < size; col++) {
      list.add(buildBlockUnit(row, col));
    }
    return list;
  }

  void _playOneStep() {
    setState(() {
      table[currentR][currentC].icon = 'null';
      int st = ((currentR * size) + (currentC % size));
      // print("st "+st.toString());
      int choose = chooseState(st);
      // print(Q[st]);
      //print("ch "+choose.toString());
      currentC = choose % size;
      currentR = (choose / size).floor();
      if (table[currentR][currentC].icon == '+1' ||
          table[currentR][currentC].icon == '-1') {
        currentC = setC;
        currentR = setR;
        table[currentR][currentC].icon = 'play';
      } else {
        if (table[currentR][currentC].icon != 'block' ||
            table[currentR][currentC].icon != 'block2')
          table[currentR][currentC].icon = 'play';
      }
    });
  }

  @override
  void initState() {
    super.initState();
    myController.text = '0.8';
    var thread = new Thread(playOnetime);
    table = List();
    R = List();
    Q = List();
    for (int row = 0; row < size; row++) {
      List<Block> list = List();
      for (int col = 0; col < size; col++) {
        list.add(
            Block(value: 0, colorBackground: Color(0xffeee4d9), icon: 'null'));
      }
      table.add(list);
    }
    table[currentR][currentC].icon = 'play';
    table[1][1].icon = 'block';
    table[2][1].icon = 'block2';
    table[0][3].icon = '-1';
    table[1][3].icon = '+1';
    len = table.length * table.length;
    action = table.length;

    setReward();
    R[7][7] = 100; //reward
    // print(R);
  }

  @override
  void dispose() {
    textStatus.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: <Widget>[
                  Text('Qlearning Rate : '),
                  Container(
                    width: 40,
                    child: TextField(
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: true),
                      controller: myController,
                      onChanged: (String value) {
                        learingRate = double.parse(value);
                      },
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                      child: Text(
                          'Q(state, action) = R(state, action) + LearningRate * Max[Q(next state, all actions)]'))
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                  height: 30,
                  child: RaisedButton(
                    child: Row(
                      children: <Widget>[
                        Text('Set Start',
                            style: TextStyle(color: Colors.white)),
                        Icon(
                          Icons.place,
                          color: Colors.white,
                        )
                      ],
                    ),
                    color: Colors.black,
                    onPressed: () {
                      setSomething = 99;
                      textStatus.sink.add('Tap block to Start');
                    },
                  ),
                ),
                Container(
                  height: 30,
                  child: RaisedButton(
                    child: Row(
                      children: <Widget>[
                        Text('Set Reward',
                            style: TextStyle(color: Colors.white)),
                        Icon(
                          Icons.sentiment_satisfied,
                          color: Colors.white,
                        )
                      ],
                    ),
                    color: Colors.black,
                    onPressed: () {
                      setSomething = 1;
                      textStatus.sink.add('Tap block to Set Reward');
                    },
                  ),
                ),
                Container(
                  height: 30,
                  child: RaisedButton(
                    child: Row(
                      children: <Widget>[
                        Text('Set Punish',
                            style: TextStyle(color: Colors.white)),
                        Icon(
                          Icons.sentiment_very_dissatisfied,
                          color: Colors.white,
                        )
                      ],
                    ),
                    color: Colors.black,
                    onPressed: () {
                      setSomething = -1;
                      textStatus.sink.add('Tap block to Set Punish');
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  height: 30,
                  child: RaisedButton(
                    child: Row(
                      children: <Widget>[
                        Text('Set Block',
                            style: TextStyle(color: Colors.white)),
                        Icon(
                          Icons.do_not_disturb,
                          color: Colors.white,
                        )
                      ],
                    ),
                    color: Colors.black,
                    onPressed: () {
                      setSomething = 81;
                      textStatus.sink.add('Choose block');
                    },
                  ),
                ),
                Container(
                  height: 30,
                  child: RaisedButton(
                    child: Row(
                      children: <Widget>[
                        Text('Set Block2',
                            style: TextStyle(color: Colors.white)),
                        Icon(
                          Icons.highlight_off,
                          color: Colors.white,
                        )
                      ],
                    ),
                    color: Colors.black,
                    onPressed: () {
                      setSomething = 82;
                      textStatus.sink.add('Choose block');
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            StreamBuilder(
              stream: textStatus.stream,
              initialData: 'State',
              builder: (context, snapshot) {
                return Text(
                  snapshot.data,
                  style: TextStyle(fontSize: 25),
                );
              },
            ),
            SizedBox(
              height: 30,
            ),
            Container(
                decoration: BoxDecoration(
                    color: Color(0xffbaad9e),
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(width: 6, color: Color(0xffbaad9e))),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: buildTable(),
                )),
            SizedBox(
              height: 34,
            ),
            RaisedButton(
              child: Text('Play Greedy Move',
                  style: TextStyle(color: Colors.white)),
              color: Colors.black,
              onPressed: _playOneStep,
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    child: Text('Find path(Random)',
                        style: TextStyle(color: Colors.white)),
                    color: Colors.black,
                    onPressed: playOnetime,
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  RaisedButton(
                    child: Text('Find path(Random) Fast',
                        style: TextStyle(color: Colors.white)),
                    color: Colors.black,
                    onPressed: playFast,
                  ),
                ],
              ),
            ),
            RaisedButton(
              child:
                  Text('Random Positon', style: TextStyle(color: Colors.white)),
              color: Colors.black,
              onPressed: _randomPositon,
            ),
            RaisedButton(
              child: Text('Reset', style: TextStyle(color: Colors.white)),
              color: Colors.black,
              onPressed: () {
                setState(() {
                  for (int row = 0; row < size; row++) {
                    for (int col = 0; col < size; col++) {
                      table[row][col].value = 0;
                    }
                  }
                  for (int i = 0; i < len; i++) {
                    for (int l = 0; l < len; l++) {
                      Q[i][l] = 0;
                    }
                  }
                });
                textStatus.sink.add('Reset');
              },
            )
          ],
        )),
      ),
    );
  }
}
